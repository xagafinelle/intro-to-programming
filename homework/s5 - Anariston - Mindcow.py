
"""

Coding PA: Intro to Programming Session 5 / OOP Homework
Author: Anariston

The game is not balanced at all, but it roughly does what it is supposed to.
Lots of room for improvement here!

To do/fix:

- Negative damage is used for healing because I didn't make a separate healing
action; heals are just negative attacks targeted on oneself.
- It could do with some subclasses - attack variants or minions (Mindcalves?)
- Probably a lot of it could be done better, but it was fun to make anyway.


"""

import random

FOE_HP = 20
PLR_HP = 20


class Attack:
    def __init__(self, die, plus, chanceToHit, attackName, combatant, target):
        self.die = die
        self.plus = plus
        self.attackName = attackName
        self.combatant = combatant
        self.target = target
        self.chanceToHit = chanceToHit
        # add this attack instance to the moves list of the attacking Combatant
        combatant.moves.append(self)

    def resolveAttack(self):
        self.combatant.announceAttack(self.attackName)
        if random.random() <= self.chanceToHit:
            if self.die < 0:
                damage = -1 * (random.randint(0, -1 * self.die) + self.plus)
            else:
                damage = random.randint(0, self.die) + self.plus
            self.target.takeDmg(damage)
        else:
            print('It failed!')


class Combatant:
    def __init__(self, name, hp):
        self.name = name
        self.hp = hp
        self.maxHP = hp
        self.moves = []

    def isAlive(self):
        return self.hp > 0

    def announceAttack(self, attackName):
        print('\n{} used {}.'.format(self.name, attackName))

    def takeDmg(self, amount):
        if amount < 0:
            print('{} healed for {} hit points!'
                  .format(self.name, amount * -1))
        else:
            print('{} took {} damage!'.format(self.name, amount))
        self.hp = min(self.maxHP, self.hp - amount)
        print('{} has {} hit points remaining.'.format(self.name, self.hp))


playerName = input('Please enter your name: ')
player = Combatant(playerName, PLR_HP)

# Instantiate the enemy
foe = Combatant('Mindcow', FOE_HP)
# ... and give it some attacks
# Psychic Charge, 1d6 + 4 damage, 40% chance to hit
foe.charge = Attack(6, 4, 0.4, 'Psychic Charge',
                    foe, player)
# Milk Splash, 1d4 + 2, 60% chance to hit
foe.splash = Attack(4, 2, 0.6, 'Milk Splash',
                    foe, player)
# Meditate (heal), 1d4 + 4, 80% chance
# Confusing convention: healing uses negative dice
foe.meditate = Attack(-4, 4, 0.8, 'Cowfulness Meditation',
                      foe, foe)


# Player moves
# Psychic Assault, 1d8, 70% chance to hit
player.assault = Attack(8, 0, 0.7, 'Psychic Assault',
                        player, foe)
# Psychic Blast, 1d10 + 6, 30% chance to hit
player.blast = Attack(10, 6, 0.3, 'Psychic Blast',
                      player, foe)
# Meditate (heal), 1d4 + 4, 65% chance
# Confusing convention: healing uses negative dice
player.meditate = Attack(-4, 4, 0.65, 'Mindfulness Meditation',
                         player, player)

# Welcome the player and explain commands
print('\n{}, you have entered a psychic duel with an aggressive Mindcow!'
      .format(playerName))
print('Type "a" or "attack" to attack or "h" or "heal" to '
      'meditate and recover some psychic energy.\n')

# Initiative
foeStarts = False
if random.random() < 0.5:
    foeStarts = True
    print('The Mindcow woke up on the right side of the field this morning, '
          'so it will move first!')

# Game loop
while player.isAlive() and foe.isAlive():
    # Foe goes first half of the time
    if foeStarts:
        foe.moves[random.randint(0, len(foe.moves) - 1)].resolveAttack()

    if not player.isAlive():
        break

    # Player's move
    command = None
    while (command != 'attack' and command != 'a' and
           command != 'heal' and command != 'h'):
        command = input('What will you do? ([a]ttack or [h]eal)\n')
    # let's say that the healing move is always last in the moves list
    if command == 'attack' or command == 'a':
        move = player.moves[random.randint(0, len(player.moves) - 2)]
        move.resolveAttack()
    else:
        player.moves[-1].resolveAttack()
    if not foe.isAlive():
        break

    # Foe goes second if it didn't go first
    if not foeStarts:
        foe.moves[random.randint(0, len(foe.moves) - 1)].resolveAttack()

# wrap up
if not player.isAlive() and not foe.isAlive():
    print('\nBoth you and the Mindcow have lost consciousness.')
    print('The fate of the multiverse will depend '
          'on who is first to awaken...\n')
elif not player.isAlive():
    print('\nYou have lost consciousness, leaving the Mindcow to '
          'wreak havoc on the material plane...\n')
    print('Try again, {}!'.format(playerName))
else:
    print('\nYou have bested the Mindcow! Congratulations, {}!\n'
          .format(playerName))
