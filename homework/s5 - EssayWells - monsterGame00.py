import random
import json

def hit( attack, defence ): #CIV style odds decider
    #print('Attack odds: {} to {}'.format( attack, defence ) )
    roll = random.randint( 1, attack+defence )
    #print('Rolled: {}'.format( roll ) )
    if roll <= attack:
        #print('Hit!')
        return True
    #print('Miss!')
    return False

def odds(top, bottom):
    #odds are top out of bottom, with safeties
    if top < 1: top = 1
    delta = bottom - top
    if delta < 1: delta = 0
    return hit( top, top + delta )


def rollDamage( damage ):
    if damage < 1:
        return 1
    return random.randint( 1, damage )


class Creature: #superclass for hero and monsters
    def __init__(self, name):
        self.name=name #whoever
        self.attack=1 #to-hit
        self.defence=1 #not to be hit
        self.damage=1 #deal damage 1 to this
        self.maxhp=1 #max life
        self.hp=1 #hit points
        self.dr=0 #damage reduction (armor)
        self.regen=0 #hit point regain/round
        self.luck=0 #for heroes only
        #all these are placeholder values
        #luck is a hero stat affecting treasure find and monster avoid

    def isDead(self): #checker!
        if self.hp < 1:
            return True
        return False

    def takeDamage(self, amount, flat = False): #modify hp based on incoming damage
        actual = amount
        if not flat:
            actual = amount - self.dr
        if ( actual < 0 ):
            actual = 0
        print('{} takes {} damage'.format( self.name, actual ) )
        self.hp -= actual #damage done
        if self.isDead():
            print('{} is dead.'.format( self.name ) )
        elif self.hp < ( self.maxhp/2 ):
            print('{} is bloodied.'.format( self.name ) )

    def smoothHP(self): #for easy HP accountancy
        if self.hp > self.maxhp:
            self.hp = self.maxhp #do not exceed max

    def smoothStats(self): # in case things change #not needed yet
        #stats cannot drop below starting
        if self.attack < 1:
            self.attack = 1
        if self.defence < 1:
            self.defence = 1
        if self. damage < 1:
            self.damage = 1
        if self.maxhp < 1:
            self.maxhp = 1
        if self.dr < 0:
            self.dr = 0
        if self.regen < 0:
            self.regen = 0
        #maybe let luck go bad! :)

    def regain(self): #apply regen to hp if not dead
        if self.isDead():
            return #nothing to do
        else:
            if self.hp < self.maxhp:
                self.hp += self.regen
                self.smoothHP()

class Monster( Creature ):
    def __init__(self,name, level):
        super().__init__(name) #create as a Creature
        if level < 0:
            level = 0
        self.level = level
        if level < 4:
            self.tier = 1
        elif level < 7:
            self.tier = 2
        elif level < 10:
            self.tier = 3
        else:
            self.tier = 4
        #print('{}: Level {} (Tier {})'.format( self.name, self.level, self.tier ))

    #Monsters have a random role function which sets their stats
    #This function is based on level
    #Levels go by tiers: 1-3,4-6,7-9, and 10 is special Tier 4
    
    def rollStats( self ):
        minStat = self.tier
        maxStat = 1+self.level
        minLife = 5*(1+self.tier)
        maxLife = 10*(1+self.tier)
        #We'll keep dr and regen less than other stats
        self.attack = random.randint( minStat, maxStat )
        self.defence = random.randint( minStat, maxStat )
        self.damage = random.randint( minStat, maxStat )        
        self.maxhp = random.randint( minLife, maxLife )
        self.hp = self.maxhp
        self.dr = random.randint( 0,self.tier )
        self.regen = random.randint( 0,self.tier )
        
    def reportStats( self ):
        #Monster version is simpler
        print('{} stats:'.format( self.name ))
        #print('Level {} (Tier {})'.format(self.level, self.tier))
        print('Attack {}  Defence {}  Damage {}'.format( self.attack, self.defence, self.damage ))
        print('HP: {} / {}  DR {}  Regen {}'.format( self.hp, self.maxhp, self.dr, self.regen ))
        
        
class Hero( Creature ):
    def __init__(self,name):
        super().__init__(name) #create as a Creature
        self.score=1
        self.xp = 0 # gain exp by winning
        self.treasure = 0 # items found
        self.level = 0 #how deep did we get in the dungeon?
        print('Hero: {}'.format( self.name ))
        self.longname = self.name
        #Descriptors now
        self.mighty = False
        self.crafty = False
        self.cunning = False
        self.tough = False
        self.fierce = False
        self.lucky = False
        self.berserk = False
        #status to track when you run away
        self.flee = False


    def setup(self):
        #Placeholder values to begin with:
        self.attack = 5
        self.defence = 5
        self.damage = 10
        self.maxhp = 20
        self.hp = 20
        self.dr = 1
        self.regen = 1
        self.luck = 0

    def applyDescriptors(self):
        if self.mighty:
            self.longname += ", the Mighty"
            self.damage += 2
            self.maxhp += 5
            self.regen += 1
        if self.crafty:
            self.longname += ", the Crafty"
            self.attack += 2
            self.defence += 2
            self.luck += 1            
        if self.cunning:
            self.longname += ", the Cunning"
            self.luck += 1
            self.attack += 2
            self.damage += 2
        if self.tough:
            self.longname += ", the Tough"
            self.dr += 1
            self.maxhp += 5
            self.defence += 2
        if self.fierce:
            self.longname += ", the Fierce"
            self.regen += 1
            self.damage += 2
            self.attack += 2
        if self.lucky:
            self.longname += ", the Lucky"
            self.luck += 1
            self.defence += 2
            self.dr += 1
        if self.berserk:
            self.longname += ", the Berserk"
            self.dr += 1
            self.regen += 1
            self.maxhp += 5
        self.hp = self.maxhp

    def setDescriptor( self, d):
        if d == 1:
            self.mighty = True
        elif d == 2:
            self.crafty = True
        elif d == 3:
            self.cunning = True
        elif d == 4:
            self.tough = True
        elif d == 5:
            self.fierce = True
        elif d == 6:
            self.lucky = True
        elif d == 7:
            self.berserk = True

    def rollStats(self):
        #give the character some descriptors
        #first descriptor at random
        p = random.randint(1,7)
        #second descriptor different from first
        q = p
        while True:
            q = random.randint(1,7)
            if not q == p:
                break
        self.setDescriptor(p)
        self.setDescriptor(q)
        self.applyDescriptors()
        print('Behold {}.'.format( self.longname ))
        print('')
        #self.reportStats()
        

    def recover( self ): #Hero function: recover after a battle
        #we will regain 25% hp plus regen
        if self.hp < self.maxhp:
            print('{} catches their breath and tends their wounds.'.format( self.name ) )
            heal = self.regen + int( self.maxhp /4 )
            self.hp += heal #recovering!
            self.smoothHP()

    def reportHP( self ): # Hero knows own HP
        print(' {} HP: {} / {}'.format( self.name, self.hp, self.maxhp))

    def getScore( self ):
        score = (1+self.exp)*(1+self.treasure)*(1+self.level)
        print('Score: {}'.format( score ) )
        self.score = score

    def reportStats( self ):
        #Hero version is longer
        print('{} stats:'.format( self.name ))
        print('Attack {}  Defence {}  Damage {}'.format( self.attack, self.defence, self.damage ))
        print('HP: {} / {}  DR {}  Regen {}'.format( self.hp, self.maxhp, self.dr, self.regen ))
        print('Luck {}  XP {}  Treasure {}'.format( self.luck, self.xp, self.treasure))
        print('Reached dungeon level {}'.format(self.level))
        self.getScore()

    def getScore( self ):
        if self.level < 1: return 0 #not started
        score = (1+self.xp)*(1+self.treasure)*(self.level)
        print('Score: {}'.format( score ) )
        self.score = score

def tier1Name():
    s=[]
    s.append('Goblin')
    s.append('Orc')
    s.append('Raider')
    s.append('Gnoll')
    s.append('Kobold')
    q = random.randint(0, len(s)-1)
    return s[q]

def tier2Name():
    s=[]
    s.append('Troll')
    s.append('Ogre')
    s.append('Dark Elf')
    s.append('Fire Spirit')
    s.append('Zombie')
    q = random.randint(0, len(s)-1)
    return s[q]

def tier3Name():
    s=[]
    s.append('Wight')
    s.append('Vampire')
    s.append('Lich')
    s.append('Wraith')
    s.append('Banshee')
    q = random.randint(0, len(s)-1)
    return s[q]

def fightRound( hero, foe ):
    hHit = hit( hero.attack, foe.defence )
    fHit = hit( foe.attack, hero.attack )
    if (hHit):
        print('{} hit {}.'.format(hero.name, foe.name))
        dam = rollDamage( hero.damage )
        foe.takeDamage( dam )
    if (fHit):
        print('{} hit {}.'.format(foe.name, hero.name))
        dam = rollDamage( foe.damage )
        hero.takeDamage( dam )

    if not hHit and not fHit:
        print('They clash, but neither can wound the other.')


def fightBattle( hero, foe ):
    hero.reportStats()
    print('')
    foe.reportStats()
    #print('')
    fighting = True
    fleeing = False
    rounds = 0 #count fight rounds so we don't get stuck in stalemate forever
    
    while fighting:
        #if the combat has been long, start killing the hero faster
        if ( rounds > hero.maxhp ):
            print('Exhaustion takes its toll on the battle-weary hero.')
            fatigue = 1+int(rounds/hero.maxhp)
            hero.takeDamage(fatigue, True) #I'm evil
        rounds += 1 # counting
        print('')
        foo = input('Press 0 to flee or any key to fight on.')
        if foo == '0':
            print('{} attempts to flee.'.format( hero.name ))
            fleeing = True
            fighting = False
        #print('')
        fightRound( hero, foe)
        if hero.isDead():
            print('')
            print('So ends the tale of {}'.format(hero.longname))
            fighting = False
        elif foe.isDead():
            print('')
            print('{} is slain; glory to {}'.format( foe.name, hero.longname ))
            hero.xp += foe.level
            fighting = False
        else:
            hero.regain()
            foe.regain()
            hero.reportHP()

    if fleeing:
        print('{} flees ignominiously.'.format( hero.name ))
        hero.flee = True


def randomTrapAttack( hero ):
    n = 5 #how many traps I have
    #maybe be cleverer later with lists
    q = random.randint(1,n)
    strikes = 0
    bypass = False #ignores armour
    damage = 0
    if q == 1:
        trapString = ' a SPEAR TRAP! '
        strikes = 1
        damage = 12
    elif q == 2:
        trapString = ' a SHOWER OF ARROWS! '
        strikes = 3
        damage = 8
    elif q == 3:
        trapString = ' a CHOKING GAS CLOUD! '
        strikes = 1
        damage = 10
        bypass = True
    elif q == 4:
        trapString = ' a DEEP SPIKY PIT TRAP! '
        strikes = 1
        damage = 15
        bypass = True
    elif q == 5:
        trapString = ' a FIREBALL TRAP! '
        strikes = 4
        damage = 5
        bypass = True
    if strikes < 1:
        return #nothing to do
    scream = 'The search triggers ' + trapString
    print( scream )
    while strikes > 0 and not hero.isDead():
        strikes -= 1 #decrement
        ouch = rollDamage( damage )
        hero.takeDamage( ouch, bypass )
    

class Treasure:
    def __init__(self, item, description, tag, value):
        self.item = item
        self.description = description
        self.tag = tag
        self.value = value
        self.found = False

    def aOfB(self):
        return '{} of {}'.format( self.item, self.description )

trove = [] #this will be a list of treasure items
inventory = [] # this will be a list of the found items

def fillTrove( trove ):
    trove.clear()
    it = Treasure( 'Amethyst', 'Aggression', 'at', 1)
    #print( it.aOfB() )
    trove.append( it )
    it = Treasure('Beryl', 'Blocking', 'dr', 1)
    trove.append( it )
    it = Treasure('Corundum', 'Concealment', 'df', 1)
    trove.append( it )    
    it = Treasure('Diamond', 'Defence', 'df', 1)
    trove.append( it )
    it = Treasure('Emerald', 'Energy', 'hp', 1)
    trove.append( it )
    it = Treasure('Fluorspar', 'Finding', 'lk', 1)
    trove.append( it )
    it = Treasure('Gem', 'Gentleness', 'hp', 1)
    trove.append( it )
    it = Treasure('Jewel', 'Jabbing', 'dm', 1)
    trove.append( it )
    it = Treasure('Lodestone', 'Luck', 'lk', 1)
    trove.append( it )
    it = Treasure('Opal', 'Offence', 'at', 1)
    trove.append( it )
    it = Treasure('Pearl', 'Protection', 'dr', 1)
    trove.append( it )
    it = Treasure('Ruby', 'Revival', 'rg', 1)
    trove.append( it )
    it = Treasure('Sapphire', 'Skewering', 'dm', 1)
    trove.append( it )
    it = Treasure('Topaz', 'Targetting', 'at', 1)
    trove.append( it )
    it = Treasure('Zircon', 'Zeal', 'rg', 1)
    trove.append( it )
    #print('Trove contains {} items'.format( len(trove) ))
    #for thing in trove:
        #print( thing.aOfB() )

def reportInventory( inv ):
    print('')
    if len(inv) < 1:
        print('Inventory is empty')
        print('')
        return
    print('Inventory contains:')
    for thing in inv:
        print( thing.aOfB() )
    print('')

def findTreasure( trove ):
    #check if any treasures are available
    gotAny = False
    for thing in trove:
        if not thing.found:
            gotAny = True
            break
    if not gotAny:
        it = Treasure('Jar', 'Eyeballs', 'nn', 0)
        return it #this treasure item does nothing
    while True:
        q = random.randint( 0, len(trove)-1 )
        if trove[q].found:
            continue
        trove[q].found = True
        it = trove[q]
        return it # give back a treasure
        
def applyBonus( buff, hero ): # apply the item buff to the hero:
    if buff.tag == 'at':
        hero.attack += buff.value
    elif buff.tag == 'df':
        hero.defence += buff.value
    elif buff.tag == 'dm':
        hero.damage += buff.value
    elif buff.tag == 'hp':
        hero.maxhp += 5*buff.value
        hero.hp += 5*buff.value
    elif buff.tag == 'dr':
        hero.dr += buff.value
    elif buff.tag == 'rg':
        hero.regen += buff.value
    elif buff.tag == 'lk':
        hero.luck += buff.value        

#int main etc. :)

fillTrove( trove ) #the dungeon is full of treasure
inventory.clear() # hero owns nothing

##
##for x in range( 20 ):
##    bar = findTreasure( trove )
##    print( bar.aOfB() )



print('Welcome to Dungeon Hack X!')
print('A Python project by EssayWells')
print('Thanks to Xagafinelle for the homework assignment')
print('')

print('Our hero stands before the gates of a mighty dungeon, as so many have done before.')

nomen = ''
while nomen == '':
    nomen = input('Please name our hero:')


protag = Hero( nomen )
protag.setup()
protag.rollStats()


dlevel = 0 #dungeon level achieved
died = False #true when you die
playing = True # true while still at it


print('The dungeon is deep and said to be filled with treasures.')
print('Its upper levels have been explored by many;')
print('From its deeper levels, none has returned.')
print('Will our brave hero be the first to plumb the depths?')
print('')

print('Our hero\'s score depends on monsters killed, treasure found, and dungeon levels explored.')
print('But if they die... nobody will ever know their story.')
print('')

print('EXPLORING: on every level you must decide whether to FLEE, SEARCH or PRESS ON.')
print('FLEEING ends the game and reports a final score; our hero survives!')
print('SEARCHING gives a chance to find treasure...')
print('...but increases the chances of meeting a wandering monster...')
print('...or a devious trap!')
print('')

print('FIGHTING: on meeting a monster, combat ensues.')
print('In every round you must choose to fight on or FLEE.')
print('FLEEING requires that our hero fight one more round of combat before escaping.')
print('')

print('ENTER THE DUNGEON.')
print('')

while playing:
    dlevel += 1
    protag.level =dlevel # record progress
    searched = False
    found = False
    print('{} is on dungeon level {}'.format( protag.longname, dlevel ))
    protag.reportHP()
    protag.getScore()
    #protag.reportStats()
    #input infiniloop
    action = ''
    while True:
        action = input('Press 0 to flee; 1 to seek treasure; 2 to report stats; 3 to show inventory; any key to press on.')
        if action == '2':
            print('')
            protag.reportStats()
            print('')
        elif action == '3':
            #print('')
            reportInventory( inventory )
            print('')            
        else:
            break #continue to decision
    if action == '0':
        print('')
        print('{} has had enough and leaves the dungeon.'.format(protag.name))
        playing = False
        protag.flee = True
        break # we're done
    elif action == '1':
        print('')
        print('{} takes time to search for treasure.'.format(protag.name))
        #we use the to-hit odds thing to save effort :)
        numerator = 1 + dlevel + protag.luck
        denominator = 3 + dlevel
        searched = True
        gotTreasure = odds( numerator, denominator )
        if gotTreasure:
            found = True
            protag.treasure += 1 # don't multi-count
            print('{} has found a Treasure:'.format( protag.name ))
            it = findTreasure( trove ) #get a magic item
            itemName = it.aOfB()
            print( itemName )
            inventory.append( it ) #item goes into inventory
            applyBonus( it , protag ) # hero gets bonus
            print('')
        else:
            print('{} has searched but found nothing.'.format(protag.name))
        #add a trap chance here! :) for evil
        numerator = 1 + dlevel - protag.luck
        denominator = 3 + dlevel
        triggerTrap = odds( numerator, denominator )
        if triggerTrap:
            randomTrapAttack( protag )
        if protag.isDead():
            died = True
            playing = False
            break # dead
        
    print('')
    print('{} presses on.'.format(protag.name))
    #now check for a wandering monster
    #always encounter on dlevel 10
    if dlevel == 10:
        print('')
        print('On the deepest level lurks the foulest fiend of all!')
        print('This is the final battle.')
        antag = Monster('Dragon', dlevel)
        antag.rollStats()
        fightBattle( protag, antag )
        if protag.isDead():
            died = True
            playing = False
            break #done because dead
        elif protag.flee:
            playing = False
            break # fled to go home
        else: #won!
            playing = False
            break
    else: #not yet on level 10. Random encounter
        #chance to meet a beastie
        numerator = 1 + dlevel - protag.luck
        if searched: numerator += 1
        if found: numerator += 1
        denominator = 2 + dlevel
        metMonster = odds(numerator, denominator)
        if metMonster:
            print('')
            print('A wandering monster attacks!')
            name=''
            if dlevel < 4:
                name = tier1Name()
            elif dlevel < 7:
                name = tier2Name()
            elif dlevel < 10:
                name = tier3Name()
            antag = Monster( name , dlevel )
            antag.rollStats()
            fightBattle(protag, antag)
            if protag.isDead():
                died = True
                playing = False
                break # RIP
            if protag.flee:
                playing = False
                break # Run away
        #if we get here, either no monster, or we won
        print('')
        protag.recover() #regain some hp before next level
        print('{} finds their way safely to the next level down.'.format(protag.name))

    
#if we got here, we either died, fled or won.
print('')
if died:
    print('{} entered the dungeon, but they never came back.'.format(protag.longname))
    print('Who knows how far they got before they failed?')
elif protag.flee:
    print('{} reached Level {} and then turned back.'.format(protag.longname, protag.level))
    protag.reportStats()
    print('What treasures were brought to light?')
    reportInventory( inventory )
else:
    print('{} defeated the dungeon!'.format(protag.longname))
    protag.reportStats()   
    print('What treasures were brought to light?')
    reportInventory( inventory )



