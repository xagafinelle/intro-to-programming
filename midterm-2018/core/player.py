if __name__ == "__main__":
    raise Exception('Do not run this file directly.')

from .creature import Creature

class Player(Creature):
    def __init__(self):
        super().__init__()

    def render(self):
        return '@'
