#!/usr/bin/env python3

import core

def main():
    gs = core.GameState()
    ui = core.Ui( gs )
    ui.run() # This shouldn't return until the game ends

if __name__ == "__main__":
    main()
