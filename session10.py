
def _binarySearch( data, goal, min_idx, max_idx ):
    '''Internal binary search function.  Does the actual recursive search. A
    nice point here is that when entering this function the min_idx and max_idx
    locations will always have been checked already, so we only need to check
    the mid-point.'''

    # Find the midpoint of our search window
    mid_idx = int((min_idx+max_idx)/2)

    # If the midpoint is equal to the min or max then we've exhausted our
    # dataset and not found anything, return -1
    if mid_idx == min_idx or mid_idx == max_idx:
        return -1

    # Now check to see if the midpoint is our goal, if so, return the index
    if data[mid_idx] == goal:
        return mid_idx

    # It isn't the midpoint, now see which half of the window our result should
    # be in.  Lower half or upper half?
    if data[mid_idx] > goal:
        # It's in the lower half, search between min_idx and mid_idx
        return _binarySearch( data, goal, min_idx, mid_idx )
    else:
        # It's in the upper half, search between mid_idx and max_idx
        return _binarySearch( data, goal, mid_idx, max_idx )

def binarySearch( data, goal ):
    '''Performs a binary search on the provided data list, searching for a
    record matching goal. The data list must be in order before calling this
    function.

    This is the outer function, this calls _binarySearch to do the actual
    searching after doing a few checks and setting up the state.
    
    More could easily be added, for example, we could reject goal values outside
    of our min and max values immediately instead of searching for them.'''

    # Establish the bounds of the search window
    min_idx = 0
    max_idx = len(data)-1

    # Check to see if the window bounds contain the goal
    if data[min_idx] == goal:
        return min_idx
    elif data[max_idx] == goal:
        # On the upper limit we're not checking to see if we've found the
        # earliest possible result, something that could be added.
        return max_idx

    # Perform the search itself, this will return the first index of the first
    # record that it finds that matches goal, not the first occurance of goal
    # in the list.
    result = _binarySearch( data, goal, min_idx, max_idx )

    # Now search backwards to find the first occurance of goal in the list,
    # if we've actually found something useful at all.
    while result > 0:
        if data[result-1] == goal:
            result = result-1
        else:
            break

    # Return our result
    return result

def main():
    data = [0,2,4,4,4,4,4,6,7,8,10]
    print(binarySearch(data, 3))

if __name__ == '__main__':
    main()
