'''Debugging wrapper/entry point for session10.py. Running this will start the
debugger, setup to run the main function in session10.py. You can then setup
breakpoints, run the program, investate state and program stack, etc.'''

# Import the debugger
import pdb

# Import our main program file
import session10

# Start the debugger on the main function of our script
pdb.run('session10.main()')
