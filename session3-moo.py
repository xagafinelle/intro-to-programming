#!/usr/bin/env python3

import random

def generate_code( length, choices=3 ):
    lower = ord('a')
    upper = lower + choices - 1
    result = ''
    for j in range(0, length):
        rand_char = chr(random.randint( lower, upper ))
        result = result + rand_char
    return result

def dash_out_string( code, index ):
    return code[:index] + '-' + code[index+1:]

def read_number( prompt, lower, upper ):
    while True:
        try:
            number = input( prompt )
            number = int(number)
            if number < lower or number > upper:
                print('Input is out of range, {} to {}'.format( lower, upper ) )
            else:
                return number
        except:
            print('You did not enter a number, please try again.')

code_size = read_number('How many letters in the code? ', 2, 25)

referenceCode = generate_code( code_size, 3 )

while True:
    code = referenceCode
    guess = input('Enter your guess: ')

    # Check the length of the guess
    if len(code) != len(guess):
        print('Guess is the wrong length, enter %d letters.' % len(code) )
        continue

    # Check for success
    if code == guess:
        print('You guessed correctly!')
        break

    # Check for correct answers in correct locations
    correct = 0
    misplaced = 0
    for index in range(0, len(code)):
        if code[index] == guess[index]:
            # the letter is completely correct
            correct = correct + 1
            code = dash_out_string( code, index )
            guess = dash_out_string( guess, index )

    for letter in guess:
        if letter == '-':
            continue
        else:
            # Is the letter in the wrong place?
            index = code.find( letter )
            if index < 0:
                pass
            else:
                misplaced = misplaced + 1
                code = dash_out_string( code, index )
            

    # Provide feedback
    print('You have %d letters correct, and %d misplaced' % (correct, misplaced) )
    
