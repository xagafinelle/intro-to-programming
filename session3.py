
import random

def generate_code( length, choices=3 ):
    lower = ord('a')
    upper = lower + choices - 1
    result = ''
    for j in range(0, length):
        rand_char = chr(random.randint( lower, upper ))
        result = result + rand_char
    return result

code = generate_code( 15, 35 )
print('The code was: ' + code)


