
inputFile = open('input-test.txt', 'r')

corpus = {}

for line in inputFile:
    line = line.strip()
    for word in line.split():
        word = word.lower()
        word = word.strip(',.!?')
        if word not in corpus:
            corpus[word] = 1
        else:
            corpus[word] = corpus[word] + 1

keys = list(corpus.keys())
keys.sort()
for key in keys:
    print('{}: {}'.format( key, corpus[key] ) )

print('There are {} unique words.'.format( len(corpus) ))
