import random

class Monster:
    def __init__(self, name):
        print('Creating a new monster named {}.'.format(name))
        self.hp = 10
        self.name = name

    def attack(self):
        print('{} is attacking'.format(self.name))

    def takeDamage(self, amount):
        print('{} took {} damage'.format( self.name, amount ))
        self.hp = self.hp - amount

    def isDead(self):
        if self.hp <= 0:
            return True
        else:
            return False

class Zombie(Monster):
    def __init__(self, name):
        super().__init__(name)
        print('Creating a new zombie named {}.'.format(name))
        self.hp = self.hp * 2
        self.alive = True

    def takeDamage(self, amount):
        """
        Tells the zombie that it has taken amount damage from any source.
        """
        super().takeDamage( amount )
        if super().isDead():
            print('Zombie would be dead if a normal Monster.')
            if random.randint(1,100) <= 30:
                self.alive = False
        
    def isDead(self):
        return not self.alive

bob = Monster('Bob')
bob.attack()
bob.takeDamage( 5 )

joe = Zombie('Joe')

while not joe.isDead():
    joe.takeDamage( random.randint(1,3) )

print(bob.hp)
print(joe.hp)
