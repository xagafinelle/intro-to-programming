
class Stats:
    def __init__(self):
        self._count = 0
        self.occurances = [55, 12, -2, 108, 77]

    @property
    def count(self):
        return self._count

    @count.setter
    def count(self, new_count):
        if new_count < 0:
            raise Exception('Cannot set count to less than zero.')
        self._count = new_count

    @property
    def average(self):
        total = 0
        for o in self.occurances:
            total = total + o
        return total/len(self.occurances)

st = Stats()

print(st.count)
st.count = 55
print(st.count)
print(st.average)
st.occurances.append(107)
print(st.average)
