import time
import random

"""

Complexity: O(n)

make_garbage: O(n)

temp.sort: O(n*log(n))

crazy loop: O(n*n) => O(n^2)

"""

def make_garbage( count ):
    ret = []
    for j in range(count):
        ret.append( count-j )
#        ret.append( random.randint(0,count) )
    return ret

new_count = 10000

start = time.time()
temp = make_garbage( new_count )
end = time.time()
print('It took {} seconds to generate {} values'.format(end-start, new_count))

temp2 = temp.copy()

start = time.time()
temp.sort()
end = time.time()
print('It took {} seconds to sort {} values'.format(end-start, new_count))

def simple_sort( data ):
    while True:
        swaps = 0
        for j in range(len(data)-1):
            if data[j] > data[j+1]:
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
                swaps = swaps +1
        if swaps == 0:
            break

start = time.time()
list_of_numbers = []
for j in range(100):
    list_of_numbers.append( random.randint(0,100) )

simple_sort( list_of_numbers )
end = time.time()
print('It took {} seconds to sort {} values'.format(end-start, new_count))

