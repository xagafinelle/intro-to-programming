from cards import helpers

import pickle

print('Starting up: ' + __name__)

print(helpers.standardSuits)

try:
    inFile = open('savedcards.save', 'rb')
    deck = pickle.load(inFile)
    inFile.close()
    print('Deck loaded')
except:
    deck = helpers.generateDeck(['H','S','C','D'], ['9', '10', 'J', 'Q', 'K', 'A'])
    print('Couldn\'t load deck, generating fresh.')

print(deck)

# Do work on the deck
deck.append( deck[0] )
del deck[0]

# Saving data
outFile = open('savedcards.save', 'wb')
pickle.dump( deck, outFile )
outFile.close()

from session7b import Thing

inFile = open('thinga.data', 'rb')
thing = pickle.load( inFile )
inFile.close()

thing.doSomething(deck)
