import pickle

class Thing:
    def __init__(self):
        self.a = 71
        self.b = 'Hello world'

    def doSomething(self, things):
        print('a = {}, b = {}'.format(self.a, self.b))
        print('These are things: ' + repr(things))


ThingOne = Thing()

print(ThingOne.b)

ThingOne.doSomething([1,2,3])

out = open('thinga.data', 'wb')
pickle.dump( ThingOne, out );
out.close()
