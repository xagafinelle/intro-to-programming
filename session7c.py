from cards import helpers

import json

print('Starting up: ' + __name__)

print(helpers.standardSuits)

try:
    with open('savedcards.json', 'rt') as inFile:
        deck = json.load(inFile)
    print('Deck loaded')
except:
    deck = helpers.generateDeck(['H','S','C','D'], ['9', '10', 'J', 'Q', 'K', 'A'])
    print('Couldn\'t load deck, generating fresh.')

print(deck)

# Do work on the deck
deck.append( deck[0] )
del deck[0]

# Saving data
with open('savedcards.json', 'wt') as outFile:
    json.dump( deck, outFile )

