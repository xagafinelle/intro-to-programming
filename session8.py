
import csv

def processRecord( record ):
    '''This is a "stub" function that would process records if there was
    actually something to do.'''
    pass

def filter01():
    '''Basic, simple approach.  Read each line in from the import, parse as CSV,
    and for each line in import read every line from filter and compare the
    email address to the filter list.  If the import email address matches any
    item in filter, stop processing and skip that item.

    This has the advantage that it will use very, very little memory, no matter
    how large the input files are.  It will run in O(N^2) time, and perform a
    large number of reads on disk, which may not be desirable.'''
    with open('import.csv', 'r') as fimport:
        csvimport = csv.reader( fimport )
        for csvrecord in csvimport:
            with open('filter.txt', 'r') as ffilter:
                hasMatch = False
                for filterline in ffilter:
                    filterline = filterline.strip()
                    if csvrecord[0] == filterline:
                        hasMatch = True
                        break
                if hasMatch:
                    print('Skipped record: {}'.format(repr(csvrecord)))
                    continue
                else:
                    processRecord( csvrecord )

def filter02():
    '''Build a set data structure of all filter records first.  This requires
    enough memory to hold all items in the filter dataset.  Set operations
    appear to run in constant time, which means that this becomes O(N) time.'''
    filterdata = set()
    with open('filter.txt', 'r') as ffilter:
        for filterline in ffilter:
            filterline = filterline.strip()
            if len(filterline) == 0:
                continue
            filterdata.add( filterline )
    print('Created filter set with {} records.'.format( len(filterdata) ) )
    with open('import.csv', 'r') as fimport:
        csvimport = csv.reader( fimport )
        for csvrecord in csvimport:
            if csvrecord[0] in filterdata:
                print('Skipped record: {}'.format(repr(csvrecord)))
                continue
            else:
                processRecord( csvrecord )

def getNextFilterLine( ffilter ):
    for filterline in ffilter:
        filterline = filterline.strip()
        if len(filterline) == 0:
            continue
        return filterline
    return None

def filter03():
    '''This approach requires both input sets to be sorted prior to execution,
    but requires a fixed, small amount of memory and runs in O(N) time.'''
    with open('filter.txt', 'r') as ffilter, open('import.csv', 'r') as fimport:
        filterline = getNextFilterLine( ffilter )
        csvimport = csv.reader( fimport )
        for csvrecord in csvimport:
            while filterline is not None and filterline < csvrecord[0]:
                filterline = getNextFilterLine( ffilter )
            if filterline is not None and filterline == csvrecord[0]:
                print('Skipped record: {}'.format(repr(csvrecord)))
                continue
            else:
                processRecord( csvrecord )

filter03()
